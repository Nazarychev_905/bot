package ru.itiscloudnazarychev;

import java.io.File;
import java.util.*;

import com.azure.core.http.HttpRequest;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;
import com.sun.deploy.association.Action;
import com.sun.deploy.registration.InstallCommands;
import com.sun.org.apache.xpath.internal.operations.Variable;
import javafx.concurrent.Task;
import java.lang.Object.*;
import javax.swing.plaf.ActionMapUIResource;

/**
 * Azure Functions with HTTP Trigger.
 */
public class GetFile {
    private HttpRequest ;

    /**
     * This function listens at endpoint "/api/GetFile". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/GetFile
     * 2. curl {your host}/api/GetFile?name=HTTP%20Query
     */
    @FunctionName("GetFile")
    public static Object run(
            @HttpTrigger(name = "req", methods = {HttpMethod.GET, HttpMethod.POST}, authLevel = AuthorizationLevel.ANONYMOUS) HttpRequest request,
            final ExecutionContext context) {
        context.getLogger().info("Java HTTP trigger processed a request.");



        String name = request.getBody().orElse(query);

        if (name == null) {
            return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Please pass a name on the query string or in the request body").build();
        } else {
            return request.createResponseBuilder(HttpStatus.OK).body("Hello, " + name).build();
        }
    }
}
