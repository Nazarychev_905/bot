package ru.itiscloudnazarychev;


import com.azure.storage.blob.*;
import com.azure.storage.blob.models.BlobContainerItem;
import com.azure.storage.blob.models.BlobItem;
import com.ctc.wstx.osgi.InputFactoryProviderImpl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class function {

    public static void main(String[] args) throws IOException {

        //System.out.println("press Enter if you want to continue and see all  Containers");
        System.out.println("write directory in your system files if u want to upload this");
        String connectStr = System.getenv("AZURE_STORAGE_CONNECTION_STRING");
        Scanner sc = new Scanner(System.in);
        String localPath = sc.nextLine();
        String zero = "";
        BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().connectionString(connectStr).buildClient();
        if (localPath.equals(zero)) {


            for (BlobContainerItem blobContainerItem : blobServiceClient.listBlobContainers()) {
                System.out.println("\nContainer: " + blobContainerItem.getName());
                BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(blobContainerItem.getName());

                for (BlobItem blobItem : blobContainerClient.listBlobs()) {
                    System.out.println("\t" + blobItem.getName());
                }
            }
        } else {
            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy-mm-dd-hh-mm-ss");


            String containerName = formatForDateNow.format(dateNow);
            BlobContainerClient containerClient = blobServiceClient.createBlobContainer(containerName);

            File file = new File(localPath);

            processFilesFromFolder(file, containerClient);
        }

    }

    public static void processFilesFromFolder(File folder, BlobContainerClient containerClient) throws IOException {

        File[] folderEntries = folder.listFiles();
        for (File entry : folderEntries) {
            if (entry.isDirectory()) {
                System.out.println("go in deep ");
                processFilesFromFolder(entry, containerClient);
                System.out.println(entry.getName());
                continue;
            } else {
                File file1 = entry.getAbsoluteFile();
                BlobClient blobClient = containerClient.getBlobClient(file1.getName());
                System.out.println("start of download File --- " + entry.getName());
                blobClient.uploadFromFile(file1.getPath());

                System.out.println("\nUploading to Blob storage as blob:\n\t" + blobClient.getBlobUrl());
            }
        }
    }
}
